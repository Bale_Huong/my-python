# -*- coding: utf-8- -*-

"""
Treasure Hunter and Mysterious Villa

Version 1.01

Created by Xuebin Huang at 20:09, 03/07/2014
"""

from random import randint

vine = False
gold_bars = None

# go back to the beginning when game over or end
def restart(name):
    print "\n%s, do you want to play the game again from the beginning?" % name
    print "1. Yes."
    print "2. No."
	
    choice = raw_input("?")
	
    if choice == "1":
	    start(name)
    elif choice == "2":
	    print "\nSee you, %s!" % name
	    exit(0)
    else:
	    print "\nPlease input the number of your choice."
	    restart(name)


# game over and hint
def dead(result, hint, name):
    print "\n", result
    print "GAME OVER"
    print hint
    restart(name)


# home end
def home(name):
    print "\nLightly, you come."
    print "The same as you lightly go out."
    print "You wave your sleeves,"
    print "not bringing a piece of cloud."
    print "\nBye bye, %s!" % name
    exit(0)

	
# end game	
def ending(name):
    print "\nCongratulations, %s! You successfully get out of the villa." % name
    print "The villa is burned up by the fire."
    print "No one knows the truth and the legend continues."
    print "%s, you bring the gold you got from the villa and go home." % name
    print "Unfortunately, you find them all have turned into stones."
    print "Now, %s, you are not able to be rich, and go back to the origin." % name
    print "%s, you're planning to hunt your next target and hoping to get a great wealth there." % name
    print "\nTHE END"
    
    restart(name)


# events when run out of the villa	
def escape(name):
    print "\nAccidentally, you hit a button and start a trap."
    print "Now the villa is full of fire."
    print "The villa will soon be demolished."
    print "%s, you need to run out of the house immediately." % name
    print "Or you will be burned and killed in this room."
    print "You're looking for some tools to help you escape."
    print "And you see some ropes lying on the floor."
    print "What should you do?"
    print "1. No time to think any more. Just Run and Jump out of the window!"
    print "2. Get a rope."
    print "3. Climb down to the ground from the window."
    rope = False
    global gold_bars
    
    while True:
	    choice = raw_input("?")
		
	    if choice == "1":
		    dead("%s, you jump and fall on the ground. Then you get injured and buried by the broken villa." % name, "Hint: don't be fool!", name)
	    elif choice == "2" and not rope:
		    print "\nYou get a rope, tie one side to the shell and put the other side out of the window." 
		    print "But it seems the rope not long enough to reach the ground."
		    rope = True
	    elif choice == "2" and rope:
		    dead("%s, you get another rope and want to extend the length of the former one. But the former one is burned. You're trapped by the fire." % name,
		    "Hint: one rope is enough.", name)
	    elif choice == "3" and not rope:
		    dead("%s, you climb down the villa. But accidentally, you fall on the ground and get injured. You're buried by the broken house." % name,
		    "Hint: get a rope first.", name)
	    elif choice == "3" and rope and gold_bars in range(0, 6):
		    print "\n%s, you climb down from the window with the rope. You are still be able to jump to the ground and run although the rope is a little bit short." % name
		    ending(name)
	    elif choice == "3" and rope and gold_bars > 5:
		    dead("%s, you're holding too many gold bars. The rope breaks and you fall on the ground. You faint and get buried by the broken house." % name,
		    "Hint: don't be so greedy.", name)
	    else:
		    print "\nPlease input the number of your choice."


# events in the gold_room			
def gold_room(name):
    print "\n%s, you succeed to get in the gold room." % name
    print "And you find thousands of gold bars and a 1000 carat diamond in a box on a shell."
    print "I know your eyes are shining at the moment, %s." % name
    print "So, what's your choice?"
    print "1. Take the diamond."
    print "2. Take nothing. I'm not interested in these things."
    print "3. Take gold bars."
    global gold_bars
    choice = raw_input("?")
	
    if choice == "1":
	    dead("As soon as you touch the diamond, some toxic gas quickly spreads out in the room. %s, you lose your consciousness." % name, 
	    "Hint: don't be so greedy.", name)
    elif choice == "2":
	    print "\nOh %s! You're very generous." % name
	    escape(name)
    elif choice == "3":
	    gold = raw_input("\nHow many bars do you want to take? >>")
		
	    try:
		    gold_bars = int(gold)
	    except ValueError:
		    print "\nPlease input a number, fool!"
		    gold_room(name)
		    return None

	    if gold_bars == 0:
		    print "\nOh yeah, %s! You are generous." % name
		    escape(name)
	    elif gold_bars in range(1, 6):
		    print "\nOK. I know you have beaten your devil in your heart, %s." % name
		    escape(name)
	    else:
		    print "\nNot bad, %s, you really need the gold." % name
		    escape(name)
    else:
	    print "\nPlease input the number of your choice."
	    gold_room(name)


# passage 1 leads to gold room
def passage_1(name):
    print "\n%s, you feel something strange while you're walking through the corridor." % name
    print "The armour is moving towards you!!"
    print "Oh no! All things in this villa are weird."
    print "You find a small hole and some fissures on the wall on your right."
    print "On your left is a stair from the 1st floor."
    print "What you need to do is..."
    print "1. Rush straight!"
    print "2. Cut the armour."
    print "3. Run downstairs."
    print "4. Cut the hole and the fissures on the wall."
    armour_move = True
	
    while True:
	    choice = raw_input("?")
		
	    if choice == "1":
		    dead("%s, you rush and step on the broken floor. You fall to the hall and get seriously hurt. You lose consciousness." % name,
		    "Hint: watch your road.", name)
	    elif choice == "2" and armour_move:
		    print "\n%s, you cut the armour by the sword and it's too solid to be broken. But the armour stops." % name
		    armour_move = False
	    elif choice == "2" and not armour_move:
		    print "You are still not able to destroy it. The armour moves again."
		    armour_move = True			
	    elif choice == "3":
		    dead("%s, you run down and step on the broken steps. You fall to the fall and hurt by the fragment. You are bleeding to die." % name,
		    "Hint: don't be scared.", name)
	    elif choice == "4" and armour_move:
		    dead("\n%s, you want to break the wall, but the armour appears at your back and hit through your chest by its fingers. You die." % name,
		    "Hint: stop it first.", name)
	    elif choice == "4" and not armour_move:
		    print "\n%s, you break the wall by the sword and hide in that room. " % name
		    print "The armour moves a moment later and keeps going. It steps on the broken floor, falls to the hall and breaks."
		    print "You take a deep breath and get in this room."
		    gold_room(name)
	    else:
		    print "\nPlease input the number of your choice."


# passage 2 leads to dead
def passage_2(name):
    print "\n%s, you feel something strange while you're walking through the corridor." % name
    print "The armour is moving towards you!!"
    print "Oh no! All things in this villa are weird."
    print "You find a small hole and some fissures on the wall on your right."
    print "On your left is a stair from the 1st floor."
    print "What you need to do is..."
    print "1. Rush straight!"
    print "2. Hit the armour."
    print "3. Run downstairs."
    print "4. Hit the hole and the fissures on the wall."
    choice = raw_input("?")
	
    if choice == "1":
	    dead("%s, you rush and step on the broken floor. You fall to the hall and get seriously hurt. You lose consciousness." % name,
	    "Hint: watch your road.", name)
    elif choice == "2":
	    dead("%s, you hit the armour with your fists, but it has no effects. You are cut in two parts by its sword." % name,
	    "Hint: fist versus sword?", name)
    elif choice == "3":
	    dead("%s, you run down and step on the broken steps. You fall to the fall and hurt by the fragment. You are bleeding to die." % name,
	    "Hint: don't be scared.", name)
    elif choice == "4":
	    dead("%s, you hit the wall with your fists, but it has no effects. You hurt your hands and get killed by the armour." % name,
	    "Hint: your fists are made in iron?", name)
    else:
	    print "\nPlease input the number of your choice."
	    passage_2(name)


# events in the corridor
def corridor(name):
    print "\n%s, you walk out the room and stand on the corridor of the 2nd floor." % name
    print "You think the treasure is hidden somewhere on the 2nd floor."
    print "You look around and ensure it's safe here because you have met some strange things before."
    print "An armour, holding a sword, is standing on your right, next to the wall."
    print "Opposite to the room you went out, there is another room which is locked."
    print "On your left is a long corridor."
    print "%s, choose your next step:" % name
    print "1. Break the door and get in the other room."
    print "2. Take the sword from the armour then walk through the corridor."
    print "3. Walk through the corridor."
    choice = raw_input("?")
	
    if choice == "1":
	    dead("%s, you break the door and step in. Unfortunately, the floor is blank and you fall on the knives in the kitchen. You are bleeding to death." % name,
	    "Hint: do you want to search all the rooms?", name)
    elif choice == "2":
	    print "\n%s, you take the sword to make sure the armour won't hit you. Then you go through the corridor." % name
	    passage_1(name)
    elif choice == "3":
	    passage_2(name)
    else:
        print "\nPlease input the number of your choice."
        corridor(name)		


# events in the second floor
def second_floor(name):
    print "\n%s, you get to the second floor and enter a room." % name
    print "You find a bed, a desk, a dressing desk and a wardrobe."
    print "It's a woman's room."
    print "You decide to leave this room. But the other door is locked."
    print "As a result, you need to find the key."
    print "Where will you search?"
    print "1. Desk."
    print "2. Dressing desk."
    print "3. Wardrobe."
    print "4. Bed."
    print "5. Door."
    door_key = False
    dd_key = False
	
    while True:
	    choice = raw_input("?")
	    if choice == "1" and not dd_key:
		    print "\n%s, you find a red key in the drawer of the desk." % name
		    dd_key = True
	    elif choice == "1" and dd_key:
		    print "\nThere is nothing special there."
	    elif choice == "2" and not dd_key:
		    print "\nThe drawer of the dressing desk is locked."
	    elif choice == "2" and dd_key:
		    print "\n%s, you find a silver key in the drawer of the dressing desk." % name
		    door_key = True
	    elif choice == "3":
		    print "\nNothing special."
	    elif choice == "4":
		    print "\nA common bed."
	    elif choice == "5" and not door_key:
		    print "\nThe door is locked."
	    elif choice == "5" and door_key:
		    print "\nThe door is open."
		    corridor(name)
	    else:
		    print "\nPlease input the number of your choice."		


# events in the host room		
def host_room(name):
    print "\nNow, %s, you get in a big room with a double bed, a big desk, a big table, two wardrobes and several chairs in it." % name
    print "You see a corpse with a knife inserting in his chest, lying beside the bed."
    print "And another corpse lying in front of the door with a input machine on it."
    print "You know the treasure is behind this door but you don't know the password."
    print "What will you do next?"
    print "1. Kick the corpses to ensure they are dead."
    print "2. Search the room and the corpses to find out some hints."
    print "3. Go to the door and type the password in."
    corpses_alive = False
    pass_get = False
    correct_code = "%d%d%d%d" % (randint(0, 9), randint(0, 9), randint(0, 9), randint(0, 9))
    wrong_code = "%d%d%d%d" % (randint(0, 9), randint(0, 9), randint(0, 9), randint(0, 9))
	
    while True:
	    choice = raw_input("?")
		
	    if choice == "1" and not corpses_alive:
		    print "\nYou kick the two corpses on the ground. Nothing happens. It seems they're both dead."
		    corpses_alive = True
	    elif choice == "1" and corpses_alive:
		    dead("You've waken up the zombies and they assault you. %s, you are killed and eaten by them." % name,
		    "Hint: respect the dead people, please.", name)
	    elif choice == "2" and not pass_get and not corpses_alive:
		    print "\nYou find a paper writing '%s' from the corpse beside the bed." % correct_code
		    print "And another paper from the corpse in front of the door, having '%s' on it." % wrong_code
		    pass_get = True
	    elif choice == "2" and not pass_get and corpses_alive:
		    dead("The zombies suddenly wake up and attack you. %s, you are killed and eaten by them." % name,
		    "Hint: please respect the dead people.", name)
	    elif choice == "2" and pass_get and not corpses_alive:
		    print "\nNothing else you can get."
		    corpses_alive = True
	    elif choice == "2" and pass_get and corpses_alive:
		    dead("The two zombies have waken up and attacked you. %s, you are killed and eaten by them." % name,
		    "Hint: you have searched too much.", name)
	    elif choice == "3":
		    print "\nYou need to input the accurate password in order to open the door."
		    print "You see a keyboard under the screen on the door."
		    print "The screen shows 'PASSWORD PLEASE'"
		    password = raw_input(">>")
			
		    if password == correct_code and not corpses_alive:
			    print "\nThe screen shows 'CORRECT PASSWORD'"
			    print "Congratulations, %s!" % name 
			    gold_room(name)
		    elif password == correct_code and corpses_alive:
			    dead("You are suddenly stroke by the zombies as soon as you finish typing the password in. %s, you die." % name,
			    "Hint: don't wake up the corpses.", name)
		    else:
			    print "\nThe screen shows 'WRONG PASSOWRD'"
			    dead("%s you start the trap and get shot as you have typed a wrong password in." % name, "Hint: guess which one is true?", name)
	    else:
		    print "\nPlease input the number of the choice."


# events in garden
def garden(name):
    print "\n%s, you walk out of the dark room and get in a garden." % name
    print "Then you find a zombie dog staring at you atrociously, which is standing in front of a stair leading to the second floor."
    print "You don't want to go back."
    print "You need to find a way to deal with the dog."
    print "So, you..."
    print "1. Hit the dog."
    print "2. Run to the stair."
    print "3. Move slowly to the stair."
    print "4. Give something for it to eat."
    print "5. Find some food in the kitchen."
    meat = False
    fridge_meat = True
    dog_eat = False
    dog_faint = False
    global vine
	
    while True:
	    choice = raw_input("?")
		
	    if choice == "1":
		    dead("%s, you lose the fight and get eaten by the dog." % name, "Hint: You are so confident to fight with your fists.", name)
	    elif choice == "2" and not dog_faint:
		    dead("%s, you get caught and bitten by the dog. You lose your consciousness." % name, "Hint: don't attract its attention.", name)
	    elif choice == "2" and dog_faint:
		    dead("%s, you run and step on a broken step. You fall and hit your head on a stone. You lose consciousness." % name,
		    "Hint: don't be careless.", name)
	    elif choice == "2" and dog_eat:
		    dead("%s, you catch its attention and it attacks you. You are beaten and eaten by it." % name, "Hint: don't attract its attention.", name)
	    elif choice == "3" and not dog_faint:
		    dead("%s, you get caught by the dog although you try your best not to bother it. Unfortunately, you are still killed by it." % name,
		    "Hint: the dog is watching you.", name)
	    elif choice == "3" and dog_eat:
		    dead("%s, you walk slowly. But the dog eats very fast. It catches and bites you. You get killed." % name,
		    "Hint: better to hit it.", name)
	    elif choice == "3" and dog_faint:
		    second_floor(name)
	    elif choice == "4" and not meat:
		    print "\n%s, you have nothing to give it to eat." % name
	    elif choice == "4" and meat:
		    
			if vine:
			    print "\n%s, do you want to mix the meat and the wine then throw it to the zombie dog?" % name
			    print "1. Yes."
			    print "2. No."
			    choice = raw_input("?")
				
			    if choice == "1":
				    print "\nThe dog eats up the meat quickly then falls down. It seems fainting."
				    meat = False
				    dog_faint = True
			    elif choice == "2":
				    print "\nThe dog is eating the meat happily."
				    meat = False
			    else:
				    print "\nPlease input the number of the choice."
			else:
			    print "\nThe dog is eating the meat happily."
	    elif choice == "5" and fridge_meat:
		    print "\n%s, you find some meat in the fridge." % name
		    meat = True
		    fridge_meat = False
	    elif choice == "5" and not fridge_meat:
		    print "\nThere is no more meat in the fridge."
	    else:
		    print "\nPlease input the number of your choice."


# events in kitchen
def kitchen(name):
    print "\n%s, now you're standing in front of the dark room and see light comes in from the door some distance away." % name
    print "You decide to walk to the other door but it's too dark to find a way."
    print "A switch is on the wall next to the dark room. Maybe it can turn the lights on."
    print "%s, you decide to..." % name
    print "1. Put on the switch to turn on the lights."
    print "2. Use your own torch."
    print "3. Walk there."
    torch_on = False
	
    while True:
	    choice = raw_input("?")
		
	    if choice == "1":
		    dead("The electricity leaks when you turn on the lights. %s, you get seriously burned and die." % name,
		    "Hint: use your own one.", name)
	    elif choice == "2" and not torch_on:
		    print "\n%s, you turn on your torch." % name
		    torch_on = True
	    elif choice == "2" and torch_on:
		    print "\n%s, you turn off your torch." % name
		    torch_on = False
	    elif choice == "3" and not torch_on:
		    dead("You kick something on the ground then fall on a pair of scissors. Your blood attracts some zombie mice to come. %s, you are eaten by them" % name,
		    "Hint: you need light.", name)
	    elif choice == "3" and torch_on:
		    print "\n%s, you walk carefully and reach the other door." % name
		    garden(name)
	    else:		
			print "\nPlease input the number of your choice."


# events in bath room
def bath_room(name):
    print "\n%s, you open the door and see there is a bathtub in the room." % name
    print "Now, you know here is the bath room."
    print "You think nothing will be hidden here so you turn around, about to leave."
    print "Suddenly, you feel a pair of shinning eyes staring at you."
    print "What are you going to do, %s?" % name
    print "1. Turn around and find out what the hell it is."
    print "2. Go out and shut the door A.S.A.P."
    choice = raw_input("?")
	
    if choice == "1":
	    dead("%s, you get seriously hurt before you recognize what it is. You are eaten by it." % name, 
	    "Hint: leave.", name)
    elif choice == "2":
	    dinning_room(name)
    else:
	    print "\nPlease input the number of your choice."
	    bath_room()

		
# events in dinning room
def dinning_room(name):
    alcohol = ["Martell", "X.O.", "Maotai", "Lafie"]
    print "\nYou see a long table with several chairs beside it."
    print "There are also some dishes on the table."
    print "%s, you ensure you're standing in the dinning room." % name
    print "Now you see a room with a half-open door on your left, a dark room on your right, and many bottles of luxurious alcohol in a shell."
    print "Such as: Martell, X.O., Maotai, Lafie"
    print "So, %s, what's your next decision?" % name
    print "1. Choose a bottle of alcohol."
    print "2. Go to the half-open room."
    print "3. Go to the dark room."
    choice = raw_input("?")
    global vine
	
    if choice == "1":
	    print "\nWhich one do you pick, %s?" % name
	    print "1.", alcohol[0], "\t2.", alcohol[1], "\t3.", alcohol[2], "\t4.", alcohol[3]
	    wine = raw_input("?")
	    try:
		    w = int(wine)
	    except ValueError:
		    print "\nPlease input a number, fool!"
		    dinning_room(name)
		    return None
		
	    if not vine:
		    print "\n%s, do you want to keep it or drink it?" % name
		    print "1. Keep it."
		    print "2. Drink it to celebrate."
		    print "3. Not take any."
		    choice = raw_input("?")
		    
		    if choice == "1": 
			    print "\nYou keep a bottle of %s." % alcohol[w-1]
			    vine = True
			    dinning_room(name)
		    elif choice == "2":
			    dead("%s, you pick up a %s and enjoy it. You faint as you think you have drunk too much. You fall asleep and never wake up because the alcohol is toxic." % (name, alcohol[w-1]),
			    "Hint: %s, you're come here for treasure but not alcohol" % name, name)
		    elif choice == "3":
			    print "\n%s, you decide no to take any of them."
			    dinning_room(name)
		    else:
			    print "\nPlease input the number of the choice."
			    dinning_room(name)
	    elif vine:
		    print "\nYou want to pick one more but drop the one you've already hold. The bottle breaks and the wine spreads out on the ground."
		    vine = False
		    dinning_room(name)			
    elif choice == "2":
	    bath_room(name)
    elif choice == "3":
	    kitchen(name)
    else:
	    print "\nPlease input the number of the choice."
	    dinning_room(name)
	
			
# events in guest room
def guest_room(name):
    print "\nYou enter the room on your right."
    print "There are a small bed, a small wardrobe and a small desk in the room."
    print "You guess it's a guest room."
    print "You seem doubting whether it needs to investigate the room."
    print "What will you do next?"
    print "1. Take a look at the wardrobe."
    print "2. Search the bed."
    print "3. Check the desk."
    print "4. Nothing interesting. Go back to the hall."
    choice = raw_input("?")
	
    if choice == "1":
	    dead("There is a zombie cat hiding in the wardrobe and you frighten it. It scratches you and runs out. %s, you get poisoned and die." % name,
	    "Hint: No need to stay.", name)
    elif choice == "2":
	    dead("You find loads of worms on the bed and they bite you. %s, you soon lose your consciousness and are eaten by them." % name,
	    "Hint: No need to stay.", name)
    elif choice == "3":
	    dead("There are lots of zombie cockroaches in the locker of the desk. When you open it, they all fly out and bite you. Sorry %s, you're eaten by them." % name,
	    "Hint: No need to stay.", name)
    elif choice == "4":
	    hall(name)
    else:	
		print "\nPlease input the number of your choice."
		guest_room()		


# events in study room
def study_room(name):
    print "\n%s, you have entered the room on your left." % name
    print "You find loads of books on the book shells in the room."
    print "You are sure this is the study room and it may have something important."
    print "You are planning to search through the room."
    print "A red button on the desk attracts your attention."
    print "What are you going to do first?"
    print "1. Read all the books from the shells."
    print "2. Press the button on the desk."
    print "3. Search the ground carefully."
    choice = raw_input("?")
	
    if choice == "1":
	    dead("A spider bites you when you are paying all your attention to read books. Then %s, you fall asleep and never wake up." % name,
	    "Hint: No need to read all books.", name)
    elif choice == "2":
	    print "\nAfter you press the button, a hidden stair appears."
	    print "What's your decision?"
	    print "1. Go upstairs."
	    print "2. It may be dangerous. Check it if it is safe."
	    trap = True
		
	    while True:
		    choice = raw_input("?")
			
		    if choice == "1" and trap:
			    dead("%s, you step on the stairs and some arrows rushes out of from the wall. You get shot and die." % name,
			    "Hint: %s, don't be so careless." % name, name)
		    elif choice == "1" and not trap:
			    host_room(name)
		    elif choice == "2" and trap:
			    print "\nYou throw some books on the stairs and some arrows rushes out from the wall."
			    trap = False
		    elif choice == "2" and not trap:
			    print "\nYou throw some books on the stairs again but no arrows comes out. It seems there is no more traps."
		    else:
			    print "\nPlease input the number of your choice."
    elif choice == '3':
	    dead("%s, you hit one of the book shells by accident and it falls down. You are crushed by the fallen book shell." % name,
	    "Hint: Nothing on the ground you need.", name)
    else:
	    print "\nPlease input the number of your choice."
	    study_room(name)

		
# events in the hall
def hall(name):
    print "\n%s, great! You have made up your mind to achieve your dream." % name
    print "Now you are standing in the hall of the villa."
    print "You see a luxurious pendant lamp on your head."
    print "You ensure it's the legendary villa of the multi-billionaire which you are looking for."
    print "So, you look around the hall and find either a room on your left and right."
    print "In front of you there are a stair to the 2nd floor and another room."
    print "Which way will you go?"
    print "1. Go upstairs."
    print "2. Go to the room in front of you."
    print "3. Go to the room on your left."
    print "4. Go to the room on your right."
    choice = raw_input("?")
	
    if choice == "1":
	    dead("%s, you walk on a broken step and fall on the ground when you're going upstairs. Then you fall on the ground and get hurt by the fragment. You bleed to death." % name,
	    "Hint: Sometimes it's not a good choice to go straight.", name)
    elif choice == "2":
	    dinning_room(name)
    elif choice == "3":
	    study_room(name)
    elif choice == "4":
	    guest_room(name)
    else:
	    print "\nPlease input the number of your choice."
	    hall(name)

		
# the beginning of the story
def start(name):
    print "\nHello, %s, welcome to the virtual world." % name
    print "%s, you're a famous treasure hunter in the world." % name
    print "One day, you heard of somebody talked about a story of a legendary multi-billionaire."
    print "You have learned that he has been lost for several years, since he went to his mysterious villa in the mountain."
    print "And you have known that he has hidden tons of wealth in his villa."
    print "You did a lot of research and finally found out where the villa is."
    print "You made up your mind to go and hunt the wealth there."
    print "Now, %s, you're standing in front of the mysterious villa." % name
    print "But it seems old and horrible."
    print "So, let me ask you a question."
    print "Do you really go into the villa and get the treasure, no matter how dangerous it is?"
    print "1. Yes! That's what I come here for."
    print "2. No! It looks ugly. I will go home."
    choice = raw_input("?")
	
    if choice == "1":
	    print "\nAll right. I know your determination. So let's explore."
	    hall(name)
    elif choice == "2":
	    print "\nOh no! How weak you are!"
	    home(name)
    else:
	    print "\nPlease input the number of your choice."
	    start(name)
