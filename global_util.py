from PyQt4 import QtCore, QtGui, uic
import time

class Event(QtCore.QObject):
    # static class variables
    count = 0
    total = 100
    progress_signal = QtCore.pyqtSignal([int])
    append_message_signal = QtCore.pyqtSignal([str])

    def __init__(self):
	QtCore.QObject.__init__(self)

    def reset(self, total):
	self.total = total
	self.count = 0
	self.progress_signal.emit(0)

    def connect_progress(self, slot):
	self.progress_signal.connect(slot)

    def connect_append_message(self, slot):
	self.append_message_signal.connect(slot)

    def sleep(self, seconds):
	self.progress()
	time.sleep(seconds)

    def progress(self):
	self.count += 1
	percent = int(self.count/float(self.total)*100)
	self.progress_signal.emit(percent)

    def append_message(self, message):
	self.append_message_signal.emit(message)

    def finish(self):
	self.progress_signal.emit(100)

class Dictionary:
    dict = {}

    @classmethod
    def reset(cls):
	cls.dict = {}

    @classmethod
    def measure_elapsed_time(cls, customer):
	key = str(customer)
	if key in cls.dict:
	    return time.clock() - cls.dict[key]
	else:
	    return -1

    @classmethod
    def measure_elapsed_time_and_return_string(cls, customer):
	t = cls.measure_elapsed_time(customer)
	return "waited for %g seconds" % t

    @classmethod
    def record_start_time(cls, customer):
	key = str(customer)
	cls.dict[key] = time.clock()

event = Event()
