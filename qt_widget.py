import sys
from PyQt4 import QtCore, QtGui, uic
import threading
import datetime
import time
import Queue
import global_util
from assignment_ck import *

class MyWidget(QtCore.QObject):
    def __init__(self):
        QtCore.QObject.__init__(self)
	
        # Set up the user interface from Designer.
        self.ui = uic.loadUi("./qt/widget.ui") # widget.ui is generated by Qt Creator
        self.ui.show()
        
        ## New-style Signal and Slot
        self.ui.startButton.clicked.connect(self.startButton_click)
	self.ui.closeButton.clicked.connect(self.ui.close)
	global_util.event.connect_progress(self.ui.progressBar.setValue)
	global_util.event.connect_append_message(self.ui.textEdit.append)
	Execution.append_message_slot = self.ui.textEdit.append

    def startButton_click(self):
	self.ui.textEdit.clear()
	self.ui.progressBar.setValue(0)
	global_util.event.reset(self.ui.spinBox.value())
	Execution.customers_amount = self.ui.spinBox.value()
	
	# start assignment_ck in a thread
	threading.Thread(target=run_assignment_ck).start()

class Execution(QtCore.QObject):
    # static class variables
    customers_amount = 0
    append_message_slot = None

    # Qt signal for appending messages to textEdit
    append_message = QtCore.pyqtSignal([str])

    def __init__(self):
	QtCore.QObject.__init__(self)
	
	# connect signal to slot if slot is not None
	if self.append_message_slot is not None:
	    self.append_message.connect(self.append_message_slot)		

    # 1 queue opens
    def queue1(self):
	################################################################	
	# get customers_amount from static variable
	customers_amount = self.customers_amount

	# print to console and textEdit
	print "customers_amount =", customers_amount
	self.append_message.emit("customers_amount = " + str(customers_amount))
	################################################################
	
	customers_pool = []
	threads = []
	results = []
	Q_a = Queue.Queue(6)
	Q_left = Queue.Queue()
	
	customers_come = Customer(customers_pool, customers_amount)
	customers_line = Queues(customers_pool, Q_a)
	customers_leave = Queues(customers_pool, Q_left)
	checkout_a = CheckOut("Queue A", Q_a, results)
	    
	main_starttime = datetime.datetime.now()
	customers_come.start()
	    
	time.sleep(0.5)
	checkout_a.start()
	threads.append(customers_come)
	threads.append(checkout_a)
	
	################################################################
	# print to console and textEdit
	print "Calculating. Please wait......"
	self.append_message.emit("Calculating. Please wait......")
	################################################################
	
	while customers_amount > 0:
	    customers_amount -= 1
	    if not Q_a.full():
		customers_line.joinqueue()
	    else:
		customers_leave.joinqueue()
	
	for t in threads:
	    t.join()    
	    
	main_endtime = datetime.datetime.now()
	main_time = (main_endtime - main_starttime).seconds
	
	################################################################
	# print to console and textEdit
	print "The entire process has last %i seconds." % main_time
	print "There are %i products have been checked out." % results[0][2]
	print "The number of average products per trolley is", results[0][2] / results[0][1]
	print "The total and average utilization of one queue is %i seconds." % results[0][3]
	print "Customers left:", Q_left.qsize()
	self.append_message.emit("The entire process has last %i seconds." % main_time)
	self.append_message.emit("There are %i products have been checked out." % results[0][2])
	self.append_message.emit("The number of average products per trolley is " + str(results[0][2] / results[0][1]))
	self.append_message.emit("The total and average utilization of one queue is %i seconds." % results[0][3])
	self.append_message.emit("Customers left: " + str(Q_left.qsize()))
	################################################################

def run_assignment_ck():
    # clear time records
    global_util.Dictionary.reset()
    
    exe = Execution()
    exe.queue1()
    
    # set progressBar to 100%
    global_util.event.finish()

def run_qt():
    app = QtGui.QApplication(sys.argv)
    window = MyWidget()
    sys.exit(app.exec_())

if __name__ == '__main__':
    # run Qt in main thread.
    run_qt()
