# -*- coding: utf-8- -*-

import threading
import datetime
import time
import Queue
from random import randint

################################################################
# edited by Joe
import global_util
################################################################

class Customer(threading.Thread):
    
    """
    Simulate the process that customers buy things 
    and append customers in customers pool.
    """
    
    def __init__(self, customers, amount):
	    threading.Thread.__init__(self)
	    self.customers = customers
	    self.amount = amount		
    
    def run(self):
        product = Trolley()
        count = 0
        #print self.amount
        for count in range(0, self.amount):
            count += 1
            self.customers.append(product.getproduct())
			
			
class Trolley(object):

    """
    Simulate the customers put products in the trolley.
    """

    def getproduct(self):
	    count = 0
	    trolley = []
	    for count in range(0, randint(1, 20)):
		    count += 1
		    product = randint(1, 6)
		    trolley.append(product)
	    return trolley
	    
class Queues(object):
    
    """
    Simulate a customer joins a queue or leaves.
    """
    
    def __init__(self, customers, q):
	    self.customers = customers
	    self.q = q
	
    def joinqueue(self):
	    try:
	        self.q.put(self.customers[0])
		
		################################################################
		# edited by Joe
		global_util.Dictionary.record_start_time(self.customers[0])
		################################################################
		
	        del self.customers[0]
	        #print "Queue length:", self.q.qsize()
	    except IndexError:
	        print "No customer."
			
class CheckOut(threading.Thread):
    
    """
    Simulate the process of check-out.
    """
    
    def __init__(self, name, q, result):
        threading.Thread.__init__(self)
        self.name = name
        self.q = q
        self.result = result
    
    def run(self):
	    #q_starttime = datetime.datetime.now()
	    customers_ck = 0
	    products_ck = 0
	    customer_checkout_time = 0
	    total_checkout_time = 0
	    while True:
	        try:
	            customer = self.q.get(True, 0.5)
		    
		    ################################################################
		    # edited by Joe
		    text = global_util.Dictionary.measure_elapsed_time_and_return_string(customer)
		    print customer, text
		    global_util.event.append_message(str(customer) + " " + text)
		    ################################################################
		    
	            for p in customer:
	                customer_checkout_time += p
	                products_ck += 1
		    
		    ################################################################
		    # edited by Joe
	            global_util.event.sleep(customer_checkout_time / 100)
		    ################################################################
		    
	            total_checkout_time += (customer_checkout_time / 100)
	            customers_ck += 1
	        except Queue.Empty:
			    #q_endtime = datetime.datetime.now()
			    #thread_time = (q_endtime - q_starttime).microseconds
			    results_ck = [self.name, customers_ck, products_ck, total_checkout_time]
			    self.result.append(results_ck)
			    break
			
class execution(object):

    """
    execute the situation
    """

    def getinput(self):
	    amount = 0
	    num = 0
	    while amount == 0:
		    input = raw_input("Please input how many customers will go to store?>>")		    
		    try:
			    num = int(input)
		    except ValueError:
			    print "Value error. Please input again."
		
		    if num > 0 and num <= 100:
			    amount = num
		    else:
			    print "Number out of range. Please input again."
		
	    #amount = num_pass			
	    return amount
		
    # 1 queue opens
 
    def queue1(self):
	    get_amount = execution()
	    customers_amount = get_amount.getinput()
	    customers_pool = []
	    threads = []
	    results = []
	    Q_a = Queue.Queue(6)
	    Q_left = Queue.Queue()
	    
	    customers_come = Customer(customers_pool, customers_amount)
	    customers_line = Queues(customers_pool, Q_a)
	    customers_leave = Queues(customers_pool, Q_left)
	    checkout_a = CheckOut("Queue A", Q_a, results)
		
	    main_starttime = datetime.datetime.now()
	    customers_come.start()
		
	    time.sleep(0.5)
	    checkout_a.start()
	    threads.append(customers_come)
	    threads.append(checkout_a)
	    print "Calculating. Please wait......"
		
	    while customers_amount > 0:
		    customers_amount -= 1
		    if not Q_a.full():
			    customers_line.joinqueue()
		    else:
			    customers_leave.joinqueue()
				
	    for t in threads:
	        t.join()
		
	    main_endtime = datetime.datetime.now()
	    main_time = (main_endtime - main_starttime).seconds
	    print "The entire process has last %i seconds." % main_time	
	    print "There are %i products have been checked out." % results[0][2]
	    print "The number of average products per trolley is", results[0][2] / results[0][1]
	    print "The total and average utilization of one queue is %i seconds." % results[0][3]
	    print "Customers left:", Q_left.qsize()
	    
if __name__ == '__main__':
    
    ################################################################
    # edited by Joe
    global_util.Dictionary.reset()
    ################################################################
    
    exe = execution()
    exe.queue1()
